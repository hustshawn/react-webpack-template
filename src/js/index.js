import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import '../styles/style.scss'

class ViewContainer extends Component {

  render() {
    return (
      <h1 className="title">{`Hello World from ViewContainer ${1+2} !!!`}</h1>
    ) 
  }
}

ReactDOM.render(<ViewContainer />, document.getElementById('app'))

